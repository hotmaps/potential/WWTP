[![DOI](https://zenodo.org/badge/doi/10.5281/zenodo.4687628.svg)](https://doi.org/10.5281/zenodo.4687628)

# EU28 Waste Water Treatment Plants

In this repository is published a small subset of the [Waterbase - UWWTD: Urban Waste Water Treatment Directive – reported data](https://www.eea.europa.eu/data-and-maps/data/waterbase-uwwtd-urban-waste-water-treatment-directive-4), containing the position of the plants projected in EPSG:3035, the designed capacity in number of person equivalent and the installed power of the plants.

![alt text](https://www.eea.europa.eu/data-and-maps/figures/water-exploitation-index-plus-wei/wei-for-river-basin-districts_30380/30380_WEI_250.eps.75dpi.png/download "Water Exploitation Index plus - WEI+")


## Repository structure

Files:
```
datapackage.json  -- Datapackage JSON file with themain meta-data
data/*.csv        -- CSV data
data/*.csvt       -- Column type information
data/*.proj       -- Projection information in PROJ.4 format
```

## Description

The present task provides data with following characteristics:

<table>
  <tr>
    <td> </td>
    <td>Spatial resolution</td>
    <td>Temporal resolution</td>
  </tr>
  <tr>
    <td>Wastewater</td>
    <td>EU28</td>
    <td>yearly</td>
  </tr>
</table>

**Table 1.** Characteristics of data provided within Task 2.6 Renewable energy sources data collection and potential review.

In this task, we collected and re-elaborated data on energy potential of renewable sources at national level, in order to build datasets for all EU28 countries at NUTS3 level. We considered the following renewable sources: biomass, waste and wastewater, shallow geothermal, wind, and solar energy. These data will be used in the toolbox to map the sources of renewable thermal energy across the EU28 and support energy planning and policy.

### Wastewater

#### Methodology

This indicator considers the thermal recovery from wastewater treatment plants, not from the sewer system. Data on the energy potential of wastewater treatment plants have been drawn from the European Environment Agency database. For each plant in the EU28 zone, geographical coordinates and capacity in population equivalent (PE) are available. In order to calculate the actual energy potential from wastewater treatment only plants suitable for energy generation must be considered. Certain plants are more suitable for energy generation than others, and suitability has been determined according to the plant capacity and its proximity to urban areas [146], as described in Figure 48.
 
Based on their capacity, expressed in population equivalent (PE), plants can be divided in four classes:

* 2 000 – 5 000 PE
* 5 001 – 50 000 PE
* 50 001 – 150 000 PE
* 150 000 PE

The proximity to urban areas has been defined following three categories

* Within the settlement: the plant is located up to 150 m from the nearest urban area, which must cover at least 25 ha within the 1000 m radius around the plant;
* Near the settlement: the plant counts at least 25 ha of urban areas within the radius between 150 m and 1000 m around it;
* Far from the settlement, the plant is located in an area where a 1000 m radius drawn around it does not contain significant shares of urban areas.

In this approach, we established that urban areas are those characterized by the following classes of the Corine Land Cover:

1. Continuous urban fabric (class 1.1.1.)
2. Discontinuous urban fabric (class 1.1.2.)
3. Industrial or commercial units (class 1.2.1.)

The thermal potential of each plant in kW was computed by multiplying the heating value of wastewater (c) by the difference in temperature (ΔT) by the volume of water flowing through the plant (V), as described by the following equation (5).

```
P = c * ΔT * V      (5)
```

We assumed the thermal capacity was equal to 1.16 kWh/m3/K [146]. The average wastewater temperature in the heating period is estimated at 10 °C. The wastewater in the effluent would be cooled down to 5°C, so that 5 K can be extracted.  The volume V is computed under the assumption of a daily flow rate (D) per person into the collector system equal to 200 l/day/inhabitant and 18 equivalent hours per day (n) – see equation 6:

```
V = PE * D/n       (6)
```

The new dataset containing the geographical coordinates, capacity and power of each wastewater treatment plant was transferred into Grass GIS, together with a raster file including classes 111, 112 and 121 of the Corine Land Cover. The suitability of the plants for the generation of thermal energy was determined by creating a query on plant capacity and proximity to urban areas. The latter was tested by creating two types of buffer, respectively of 150 and 1000 m radius, around the plants, and counting the hectare of urban areas within these buffers.

The map shown in Figure 49 classifies the plants by colour according to their suitability for energy generation. The map shown in Figure 49 classifies the plants by colour according to their suitability for the production of thermal energy. Green dots represent plants where energy production is feasible; yellow dots represent plants where energy production is feasible under certain conditions; red dots represent plants where energy production is not feasible.

The grid of suitable plants was then crossed with the structure of EU28 at NUTS3 level, in order to determine the potential of wastewater treatment plants in each province. Grass GIS allowed us to count the plants for each NUTS3 area and to sum up their energy potential, previously calculated, giving in the end the total potential from wastewater for each NUTS3 region. A second color-coded map represents the potential for each province (see Figure 49).

## Limitations of data

The data here calculated are estimations of the energy potential from renewable energy sources. The hypotheses we made when deciding what data to consider, when re-elaborating the data at more aggregated territorial levels and finally when deciding how to convey the results, can influence the results.

In some cases, we underestimated the actual potential (biomass), by downscaling the available resource for sustainability reasons, in others, we overestimated the potential (wind, solar) due to our assumption of using all available areas, according only to some GIS sustainable criteria, where energy generation is feasible without considering economic profitability.

The potentials here reported **do not account for any type of energy conversion**: when estimating the actual potential, the user will need to choose the technology through which the potential can be exploited (for example COP for the wastewater treatment plant or the efficiency for solar thermal, photovoltaic and wind).

For these reasons, the data must be considered **as indicators, rather than absolute figures** representing the actual energy potential of renewable sources in a territory.

## License

Copyright © 2016-2018: Giulia Garegnani <giulia.garegnani@eurac.edu>,  Pietro Zambelli <pietro.zambelli@eurac.edu>
 
Creative Commons Attribution 4.0 International License
This work is licensed under a Creative Commons CC BY 4.0 International License.

SPDX-License-Identifier: CC-BY-4.0
License-Text: https://spdx.org/licenses/CC-BY-4.0.html

## How to cite it

@article{SCARAMUZZINO20191,
title = "Integrated approach for the identification of spatial patterns related to renewable energy potential in European territories",
journal = "Renewable and Sustainable Energy Reviews",
volume = "101",
pages = "1 - 13",
year = "2019",
issn = "1364-0321",
doi = "https://doi.org/10.1016/j.rser.2018.10.024",
url = "http://www.sciencedirect.com/science/article/pii/S1364032118307275",
author = "Chiara Scaramuzzino and Giulia Garegnani and Pietro Zambelli"
}

## Acknowledgement

We would like to convey our deepest appreciation to the Horizon 2020 [Hotmaps Project](http://www.hotmaps-project.eu/) (Grant Agreement number 723677), which provided the funding to carry out the present investigation.

We acknowledge the [European Environmental Agency](https://www.eea.europa.eu/) that provide the original dataset used to generate the dataset contained in this repository
